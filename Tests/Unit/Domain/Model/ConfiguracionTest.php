<?php

namespace FranciscoYanquin\GoogleDrive\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Francisco Yanquin <f.yanquin01@ufromail.cl>, Ceisufro
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \FranciscoYanquin\GoogleDrive\Domain\Model\Configuracion.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Francisco Yanquin <f.yanquin01@ufromail.cl>
 */
class ConfiguracionTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \FranciscoYanquin\GoogleDrive\Domain\Model\Configuracion
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \FranciscoYanquin\GoogleDrive\Domain\Model\Configuracion();
	}

	public function tearDown()
	{
		unset($this->subject);
	}



	/**
	 * @test
	 */
	public function getUrlServicioReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getUrlServicio()
		);
	}

	/**
	 * @test
	 */
	public function setUrlServicioForStringSetsUrlServicio()
	{
		$this->subject->setUrlServicio('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'urlServicio',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getUbicacionArchivoReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getUbicacionArchivo()
		);
	}

	/**
	 * @test
	 */
	public function setUbicacionArchivoForStringSetsUbicacionArchivo()
	{
		$this->subject->setUbicacionArchivo('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'ubicacionArchivo',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getIdCarpetaReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getIdCarpeta()
		);
	}

	/**
	 * @test
	 */
	public function setIdCarpetaForStringSetsIdCarpeta()
	{
		$this->subject->setIdCarpeta('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'idCarpeta',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getIdClienteReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getIdCliente()
		);
	}

	/**
	 * @test
	 */
	public function setIdClienteForStringSetsIdCliente()
	{
		$this->subject->setIdCliente('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'idCliente',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getClaveSecretaReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getClaveSecreta()
		);
	}

	/**
	 * @test
	 */
	public function setClaveSecretaForStringSetsClaveSecreta()
	{
		$this->subject->setClaveSecreta('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'claveSecreta',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getUrlRedireccionReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getUrlRedireccion()
		);
	}

	/**
	 * @test
	 */
	public function setUrlRedireccionForStringSetsUrlRedireccion()
	{
		$this->subject->setUrlRedireccion('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'urlRedireccion',
			$this->subject
		);
	}
}
