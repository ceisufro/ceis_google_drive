<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 07-10-16
 * Time: 17:26
 */

namespace FranciscoYanquin\GoogleDrive\Util;

class GestionDrive
{

    private $id_usuario;
    private $clave_usuario;
    private $url_redireccion;
    private $servidor;
    private $user_id;

    public function __construct($id_usuario,$clave_usuario,$url_redireccion) {

        $this->id_usuario = $id_usuario;
        $this->clave_usuario = $clave_usuario;
        $this->url_redireccion = $url_redireccion;
        $this->servidor = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_REQUEST_DIR');
        $this->user_id = $user_id = $GLOBALS['TSFE']->fe_user->user['ses_userid'];
    }


    public function subirArchivo($nombre_archivo, $ubicacion_archivo, $id_carpeta_destino) {

        if (isset($this->user_id)) {
            if(isset($_GET["code"])) {
                $codigoAcceso = $_GET["code"];
                error_log("Codigo de acceso obtenido: ".$_GET["code"]);

                $arregloServicioRedireccion = array(
                    "codigoAcceso" => $codigoAcceso,
                    "ubicacionArchivo"=> $ubicacion_archivo,
                    "nombreArchivo" => $nombre_archivo,
                    "idCarpeta" => $id_carpeta_destino,
                    "idCliente" => $this->id_usuario,
                    "claveCliente" => $this->clave_usuario,
                    "urlRedireccion" => $this->url_redireccion,
                    "user_id" => $this->user_id
                );

                $servicio = curl_init($this->servidor."index.php?eID=redirigeLogin");
                curl_setopt($servicio, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($servicio, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($servicio, CURLOPT_POSTFIELDS,http_build_query($arregloServicioRedireccion));

                $respuesta = curl_exec($servicio);

                if($respuesta == "Credencial guardada") {
                    error_log("Se guardo o actualizo el token de acceso");

                    $arregloServicioSubida = array(
                        "ubicacionArchivo"=> $ubicacion_archivo,
                        "nombreArchivo" => $nombre_archivo,
                        "idCarpeta" => $id_carpeta_destino,
                        "idCliente" => $this->id_usuario,
                        "claveCliente" => $this->clave_usuario,
                        "urlRedireccion" => $this->url_redireccion,
                        "user_id" => $this->user_id
                        );

                    $servicio = curl_init($this->servidor."index.php?eID=subeArchivo");
                    curl_setopt($servicio, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($servicio, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($servicio, CURLOPT_POSTFIELDS,http_build_query($arregloServicioSubida));

                    $respuesta = curl_exec($servicio);
                    error_log($respuesta);
                    return $respuesta;
                }

            } else {
                $arregloServicioCredencial = array(
                    "ubicacionArchivo"=> $ubicacion_archivo,
                    "nombreArchivo" => $nombre_archivo,
                    "idCarpeta" => $id_carpeta_destino,
                    "idCliente" => $this->id_usuario,
                    "claveCliente" => $this->clave_usuario,
                    "urlRedireccion" => $this->url_redireccion,
                    "user_id" => $this->user_id);

                $servicio = curl_init($this->servidor."index.php?eID=solicitaCredencial");
                curl_setopt($servicio, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($servicio, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($servicio, CURLOPT_POSTFIELDS,http_build_query($arregloServicioCredencial));
                //curl_setopt($servicio, CURLOPT_POSTFIELDS,http_build_query());

                $respuesta = curl_exec($servicio);

                if($respuesta == "true") {
                    $arregloServicioSubida = array(
                        "ubicacionArchivo"=> $ubicacion_archivo,
                        "nombreArchivo" => $nombre_archivo,
                        "idCarpeta" => $id_carpeta_destino,
                        "idCliente" => $this->id_usuario,
                        "claveCliente" => $this->clave_usuario,
                        "urlRedireccion" => $this->url_redireccion,
                        "user_id" => $this->user_id);

                    $servicio = curl_init($this->servidor."index.php?eID=subeArchivo");
                    curl_setopt($servicio, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($servicio, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($servicio, CURLOPT_POSTFIELDS,http_build_query($arregloServicioSubida));

                    $respuesta = curl_exec($servicio);
                    error_log($respuesta);
                    return $respuesta;
                } else {
                    header('Location: ' . $respuesta);
                }
            }

        } else {
            error_log("No ha iniciado sesion");
        }
    }

    public function listarArchivos() {
        if (isset($this->user_id)) {
            if(isset($_GET["code"])) {
                $codigoAcceso = $_GET["code"];
                error_log("Codigo de acceso obtenido: ".$_GET["code"]);

                $arregloServicioRedireccion = array(
                    "codigoAcceso" => $codigoAcceso,
                    "idCliente" => $this->id_usuario,
                    "claveCliente" => $this->clave_usuario,
                    "urlRedireccion" => $this->url_redireccion,
                    "user_id" => $this->user_id
                );

                $servicio = curl_init($this->servidor."index.php?eID=redirigeLogin");
                curl_setopt($servicio, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($servicio, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($servicio, CURLOPT_POSTFIELDS,http_build_query($arregloServicioRedireccion));

                $respuesta = curl_exec($servicio);

                if($respuesta == "Credencial guardada") {
                    error_log("Se guardo o actualizo el token de acceso");

                    $arregloServicioListado = array(
                        "idCliente" => $this->id_usuario,
                        "claveCliente" => $this->clave_usuario,
                        "urlRedireccion" => $this->url_redireccion,
                        "user_id" => $this->user_id);

                    $servicio = curl_init($this->servidor."index.php?eID=listaCarpetas");
                    curl_setopt($servicio, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($servicio, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($servicio, CURLOPT_POSTFIELDS,http_build_query($arregloServicioListado));

                    $respuesta = curl_exec($servicio);
                    error_log($respuesta);
                    return $respuesta;
                }

            } else {
                $arregloServicioRedireccion = array(
                    "idCliente" => $this->id_usuario,
                    "claveCliente" => $this->clave_usuario,
                    "urlRedireccion" => $this->url_redireccion,
                    "user_id" => $this->user_id
                );
                $servicio = curl_init($this->servidor."index.php?eID=solicitaCredencial");
                curl_setopt($servicio, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($servicio, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($servicio, CURLOPT_POSTFIELDS,http_build_query($arregloServicioRedireccion));

                $respuesta = curl_exec($servicio);

                if($respuesta == "true") {
                    $arregloServicioListado = array(
                        "idCliente" => $this->id_usuario,
                        "claveCliente" => $this->clave_usuario,
                        "urlRedireccion" => $this->url_redireccion,
                        "user_id" => $this->user_id);

                    $servicio = curl_init($this->servidor."index.php?eID=listaCarpetas");
                    curl_setopt($servicio, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($servicio, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($servicio, CURLOPT_POSTFIELDS,http_build_query($arregloServicioListado));

                    $respuesta = curl_exec($servicio);
                    error_log($respuesta);
                    return $respuesta;
                } else {
                    header('Location: ' . $respuesta);
                }
            }
        } else {
            error_log("No ha iniciado sesion");
        }
    }
}