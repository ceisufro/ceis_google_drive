<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 19-12-2016
 * Time: 10:53
 */

namespace FranciscoYanquin\GoogleDrive\Util;
use MV\SocialAuth\Service\SocialAuthenticationService;


class Social_auth_slot
{

    /**
     * Solot for EXT:social_auth after user auth
     * MV\SocialAuth\Service\SocialAuthenticationService::class authUser
     *
     * @param array $user array record
     * @param int $result One of these values: 100 = Pass, 0 = Failed, 200 = Success
     * @param SocialAuthenticationService $pObj
     */
    public function authUser($user, &$result, SocialAuthenticationService $pObj)
    {
        error_log("Slot");
        if($result == 200) {
            /*
             * Crea archivo con datos de session de google
             */
            $credentialsPath = PATH_site . "fileadmin/Credencial_acceso_drive/{$user['uid']}.json";

            $tokenAcceso = array(
                "access_token"  => unserialize($_SESSION["HA::STORE"]['hauth_session.google.token.access_token']),
                "token_type"    => "Bearer",
                "refresh_token" => unserialize($_SESSION["HA::STORE"]['hauth_session.google.token.refresh_token']),
                "expires_in"    => unserialize($_SESSION["HA::STORE"]['hauth_session.google.token.expires_in']),
                "expires_at"    => unserialize($_SESSION["HA::STORE"]['hauth_session.google.token.expires_at']),
                "is_logged_in"  => unserialize($_SESSION["HA::STORE"]['hauth_session.google.is_logged_in']),
            );
            $tokenAcceso["created"] = $tokenAcceso["expires_at"] - $tokenAcceso["expires_in"];

            // Store the credentials to disk.
            if(!file_exists(dirname($credentialsPath))) {
                mkdir(dirname($credentialsPath), 0700, true);
            }
            file_put_contents($credentialsPath, json_encode($tokenAcceso));
        }
    }


}