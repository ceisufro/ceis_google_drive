<?php
namespace FranciscoYanquin\GoogleDrive\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Francisco Yanquin <f.yanquin01@ufromail.cl>, Ceisufro
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Datos de la aplicacion registrada, ubicacion de archvo y carpeta de destino
 */
class Configuracion extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * urlServicio
     *
     * @var string
     */
    protected $urlServicio = '';
    
    /**
     * ubicacionArchivo
     *
     * @var string
     */
    protected $ubicacionArchivo = '';
    
    /**
     * idCarpeta
     *
     * @var string
     */
    protected $idCarpeta = '';
    
    /**
     * idCliente
     *
     * @var string
     */
    protected $idCliente = '';
    
    /**
     * claveSecreta
     *
     * @var string
     */
    protected $claveSecreta = '';
    
    /**
     * urlRedireccion
     *
     * @var string
     */
    protected $urlRedireccion = '';
    
    /**
     * Returns the urlServicio
     *
     * @return string $urlServicio
     */
    public function getUrlServicio()
    {
        return $this->urlServicio;
    }
    
    /**
     * Sets the urlServicio
     *
     * @param string $urlServicio
     * @return void
     */
    public function setUrlServicio($urlServicio)
    {
        $this->urlServicio = $urlServicio;
    }
    
    /**
     * Returns the ubicacionArchivo
     *
     * @return string $ubicacionArchivo
     */
    public function getUbicacionArchivo()
    {
        return $this->ubicacionArchivo;
    }
    
    /**
     * Sets the ubicacionArchivo
     *
     * @param string $ubicacionArchivo
     * @return void
     */
    public function setUbicacionArchivo($ubicacionArchivo)
    {
        $this->ubicacionArchivo = $ubicacionArchivo;
    }
    
    /**
     * Returns the idCarpeta
     *
     * @return string $idCarpeta
     */
    public function getIdCarpeta()
    {
        return $this->idCarpeta;
    }
    
    /**
     * Sets the idCarpeta
     *
     * @param string $idCarpeta
     * @return void
     */
    public function setIdCarpeta($idCarpeta)
    {
        $this->idCarpeta = $idCarpeta;
    }
    
    /**
     * Returns the idCliente
     *
     * @return string $idCliente
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }
    
    /**
     * Sets the idCliente
     *
     * @param string $idCliente
     * @return void
     */
    public function setIdCliente($idCliente)
    {
        $this->idCliente = $idCliente;
    }
    
    /**
     * Returns the claveSecreta
     *
     * @return string $claveSecreta
     */
    public function getClaveSecreta()
    {
        return $this->claveSecreta;
    }
    
    /**
     * Sets the claveSecreta
     *
     * @param string $claveSecreta
     * @return void
     */
    public function setClaveSecreta($claveSecreta)
    {
        $this->claveSecreta = $claveSecreta;
    }
    
    /**
     * Returns the urlRedireccion
     *
     * @return string $urlRedireccion
     */
    public function getUrlRedireccion()
    {
        return $this->urlRedireccion;
    }
    
    /**
     * Sets the urlRedireccion
     *
     * @param string $urlRedireccion
     * @return void
     */
    public function setUrlRedireccion($urlRedireccion)
    {
        $this->urlRedireccion = $urlRedireccion;
    }

}