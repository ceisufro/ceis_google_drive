<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 07-10-16
 * Time: 18:17
 */


if (!defined ('PATH_typo3conf')) die ('Access denied.');

\TYPO3\CMS\Frontend\Utility\EidUtility::initTCA();

$id = isset($HTTP_GET_VARS['id'])?$HTTP_GET_VARS['id']:0;
header('Content-Type: application/json');

$TSFE = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController', $GLOBALS['TYPO3_CONF_VARS'], $id, '0', 1);
$GLOBALS['TSFE'] = $TSFE;
$GLOBALS['TSFE']->initFEuser(); // Get FE User Information
$GLOBALS['TSFE']->fetch_the_id();
$GLOBALS['TSFE']->getPageAndRootline();
$GLOBALS['TSFE']->initTemplate();
$GLOBALS['TSFE']->tmpl->getFileName_backPath = PATH_site;
$GLOBALS['TSFE']->forceTemplateParsing = 1;
$GLOBALS['TSFE']->getConfigArray();
$GLOBALS['TSFE']->register['hello'] = 1;

/** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');

include("vendor/autoload.php");

error_log("Se inicia servicio Listado de carpetas");

$id_usuario = $_POST["idUsuario"];
$clave_usuario = $_POST["claveUsuario"];
$url_Redireccion = $_POST["urlRedireccion"];
$user_id = $_POST['user_id'];

date_default_timezone_set('Chile/Continental');

define('APPLICATION_NAME', 'Integracion google drive typo3');
define('CREDENTIALS_PATH', '/Credencial_acceso_drive/');
//define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret.json');

$client = new Google_Client();
$client->setApplicationName(APPLICATION_NAME);
$client->setScopes(array('https://www.googleapis.com/auth/drive'));

$client->setClientId($id_usuario);
$client->setClientSecret($clave_usuario);
$client->setRedirectUri($url_Redireccion);
//$client->setAuthConfig(CLIENT_SECRET_PATH);
$client->setAccessType('offline');

$credentialsPath = PATH_site."fileadmin".CREDENTIALS_PATH.$user_id.".json";

error_log("Ruta Credencial: ".$credentialsPath);

if (file_exists($credentialsPath)) {
    $accessToken = json_decode(file_get_contents($credentialsPath), true);

    $client->setAccessToken($accessToken);

    try {

        $carpetas = array();

        $service = new Google_Service_Drive($client);

        $pageToken = null;
        do {
            $response = $service->files->listFiles(array(
                'q' => "mimeType='application/vnd.google-apps.folder' and trashed=false",
                'spaces' => 'drive',
                'pageToken' => $pageToken,
                'fields' => 'nextPageToken, files(id, name)',
            ));
            foreach ($response->files as $file) {
                $carpeta = array();
                $carpeta["nombre"] = $file->name;
                $carpeta["id"] = $file->id;
                $carpetas[] = $carpeta;
            }
        } while ($pageToken != null);

        echo json_encode($carpetas);

    } catch (Exception $exception) {
        echo "Codigo: ".$exception->getCode()." - Error: ".$exception->getMessage();
    }
} else {
    echo "Credencial inexistente";
}