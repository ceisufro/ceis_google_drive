<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 04-10-16
 * Time: 9:58
 */

if (!defined ('PATH_typo3conf')) die ('Access denied.');

\TYPO3\CMS\Frontend\Utility\EidUtility::initTCA();

$id = isset($HTTP_GET_VARS['id'])?$HTTP_GET_VARS['id']:0;
header('Content-Type: application/json');

$TSFE = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController', $GLOBALS['TYPO3_CONF_VARS'], $id, '0', 1);
$GLOBALS['TSFE'] = $TSFE;
$GLOBALS['TSFE']->initFEuser(); // Get FE User Information
$GLOBALS['TSFE']->fetch_the_id();
$GLOBALS['TSFE']->getPageAndRootline();
$GLOBALS['TSFE']->initTemplate();
$GLOBALS['TSFE']->tmpl->getFileName_backPath = PATH_site;
$GLOBALS['TSFE']->forceTemplateParsing = 1;
$GLOBALS['TSFE']->getConfigArray();
$GLOBALS['TSFE']->register['hello'] = 1;

/** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');

include("vendor/autoload.php");

error_log("Se inicia servicio de Redireccion ");

$idCliente = $_POST["idCliente"];
$claveCliente = $_POST["claveCliente"];
$urlRedireccion = $_POST["urlRedireccion"];
$user_id = $_POST["user_id"];

date_default_timezone_set('Chile/Continental');

define('APPLICATION_NAME', 'Integracion google drive typo3');
define('CREDENTIALS_PATH', '/Credencial_acceso_drive/');

$codigoAcceso = $_POST["codigoAcceso"];

$client = new Google_Client();
$client->setApplicationName(APPLICATION_NAME);
$client->addScope(['https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/plus.login']);
$client->setClientId($idCliente);
$client->setClientSecret($claveCliente);
$client->setRedirectUri($urlRedireccion);
$client->setAccessType('offline');


$credentialsPath = PATH_site."fileadmin".CREDENTIALS_PATH.$user_id.".json";

try {
    if (isset($codigoAcceso)) {

        $client->authenticate($codigoAcceso);
        $tokenAcceso = $client->getAccessToken();

        // Store the credentials to disk.
        if(!file_exists(dirname($credentialsPath))) {
            mkdir(dirname($credentialsPath), 0700, true);
        }

        file_put_contents($credentialsPath, json_encode($tokenAcceso));
        error_log("Se guardo el siguiente token de acceso: ".json_encode($tokenAcceso));
        error_log("Credential guardada en ".$credentialsPath." con nombre ".$user_id.".json");
        echo "Credencial guardada";
    } else {
        echo "Credencial no guardada";
    }

} catch (Exception $exception) {
    echo "Codigo: ".$exception->getCode()." - Error: ".$exception->getMessage();
}