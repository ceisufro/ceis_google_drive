<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 04-10-16
 * Time: 9:56
 */

if (!defined ('PATH_typo3conf')) die ('Access denied.');

\TYPO3\CMS\Frontend\Utility\EidUtility::initTCA();

$id = isset($HTTP_GET_VARS['id'])?$HTTP_GET_VARS['id']:0;
header('Content-Type: application/json');

$TSFE = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController', $GLOBALS['TYPO3_CONF_VARS'], $id, '0', 1);
$GLOBALS['TSFE'] = $TSFE;
$GLOBALS['TSFE']->initFEuser(); // Get FE User Information
$GLOBALS['TSFE']->fetch_the_id();
$GLOBALS['TSFE']->getPageAndRootline();
$GLOBALS['TSFE']->initTemplate();
$GLOBALS['TSFE']->tmpl->getFileName_backPath = PATH_site;
$GLOBALS['TSFE']->forceTemplateParsing = 1;
$GLOBALS['TSFE']->getConfigArray();
$GLOBALS['TSFE']->register['hello'] = 1;

/** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');

include("vendor/autoload.php");

error_log("Se inicia servicio Solicitud de acceso");

$idCliente = $_POST["idCliente"];
$claveCliente = $_POST["claveCliente"];
$urlRedireccion = $_POST["urlRedireccion"];
$user_id = $_POST['user_id'];

date_default_timezone_set('Chile/Continental');
define('APPLICATION_NAME', 'Integracion google drive typo3');
define('CREDENTIALS_PATH', '/Credencial_acceso_drive/');

$client = new Google_Client();
$client->setApplicationName(APPLICATION_NAME);
$client->setScopes(array('https://www.googleapis.com/auth/drive'));
$client->setClientId($idCliente);
$client->setClientSecret($claveCliente);
$client->setRedirectUri($urlRedireccion);
$client->setAccessType('offline');

// Load previously authorized credentials from a file.

$credentialsPath = PATH_site."fileadmin".CREDENTIALS_PATH.$user_id.".json";
if (file_exists($credentialsPath)) {
    $accessToken = json_decode(file_get_contents($credentialsPath), true);
    $nuevoToken = 1;
    $client->setAccessToken($accessToken);

    // Refresh the token if it's expired.
    if ($client->isAccessTokenExpired()) {
        error_log("El token expiró (Refrescar Token)");

        $nuevoToken = $client->getRefreshToken();

        if (isset($nuevoToken)) {
            $client->fetchAccessTokenWithRefreshToken($nuevoToken);

            error_log("El nuevo token es ".$nuevoToken);

            file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
        }

    } else {
        error_log("Token se mantiene: ".$accessToken['access_token']);
    }

    if(is_null($nuevoToken)) {
        error_log("Actualizacion de token no se pudo realizar. Se realiza redirecccion para obtener nuevo token de accesso");
        $authUrl = $client->createAuthUrl();
        echo $authUrl;
    } else {
        error_log("Actualizacion de token realizado correctamente");
        echo "true";
    }
} else {
    // Request authorization from the user.
    error_log("No existe credencial... ");
    $authUrl = $client->createAuthUrl();
    error_log("Se realiza redireccion a: ".$authUrl);

    echo $authUrl;
}