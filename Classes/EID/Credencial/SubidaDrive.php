<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 04-10-16
 * Time: 9:58
 */

if (!defined ('PATH_typo3conf')) die ('Access denied.');

\TYPO3\CMS\Frontend\Utility\EidUtility::initTCA();

$id = isset($HTTP_GET_VARS['id'])?$HTTP_GET_VARS['id']:0;
header('Content-Type: application/json');

$TSFE = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController', $GLOBALS['TYPO3_CONF_VARS'], $id, '0', 1);
$GLOBALS['TSFE'] = $TSFE;
$GLOBALS['TSFE']->initFEuser(); // Get FE User Information
$GLOBALS['TSFE']->fetch_the_id();
$GLOBALS['TSFE']->getPageAndRootline();
$GLOBALS['TSFE']->initTemplate();
$GLOBALS['TSFE']->tmpl->getFileName_backPath = PATH_site;
$GLOBALS['TSFE']->forceTemplateParsing = 1;
$GLOBALS['TSFE']->getConfigArray();
$GLOBALS['TSFE']->register['hello'] = 1;

/** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');

include("vendor/autoload.php");

error_log("Se inicia servicio subida de archivo");

$ubicacionArchivo = $_POST["ubicacionArchivo"];
$nombreArchivo = $_POST["nombreArchivo"];
$carpetaDestino = $_POST["idCarpeta"];
$idCliente = $_POST["idCliente"];
$claveCliente = $_POST["claveCliente"];
$urlRedireccion = $_POST["urlRedireccion"];
$user_id = $_POST['user_id'];

date_default_timezone_set('Chile/Continental');

define('APPLICATION_NAME', 'Integracion google drive typo3');
define('CREDENTIALS_PATH', '/Credencial_acceso_drive/');
//define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret.json');

$client = new Google_Client();
$client->setApplicationName(APPLICATION_NAME);
$client->setScopes(array('https://www.googleapis.com/auth/drive','https://www.googleapis.com/auth/drive.file'));
//$client->setAuthConfig(CLIENT_SECRET_PATH);
$client->setClientId($idCliente);
$client->setClientSecret($claveCliente);
$client->setRedirectUri($urlRedireccion);
$client->setAccessType('offline');

// Load previously authorized credentials from a file.

$credentialsPath = PATH_site."fileadmin".CREDENTIALS_PATH.$user_id.".json";

if (file_exists($credentialsPath)) {
    error_log("Credencial y token de acceso existen");
    $accessToken = json_decode(file_get_contents($credentialsPath), true);

    $client->setAccessToken($accessToken);

    try {
        $service = new Google_Service_Drive($client);

        $rutaArchivo = PATH_site.$ubicacionArchivo;

        $data = file_get_contents($ubicacionArchivo);

        //Insert a file
        $datosArchivo = new Google_Service_Drive_DriveFile(array(
            'name' => $nombreArchivo,
            'parents' => array($carpetaDestino)
        ));

        //Archivos existentes en la carpeta
        $files = $service->files->listFiles([
            "q" => "'".$carpetaDestino."' in parents and trashed=false",
        ]);

        $archivoExistente = false;

        //Se buscan coincidencias del archivo que se quiera subir al servidor
        //Si el archivo ya exista, este se elimina y se sube la nueva version
        foreach ($files->files as $file) {
            if ($nombreArchivo == $file->name) {
                $service->files->delete($file->id);
                error_log("Se elimino una copia del archivo existente en la cuenta");
                break;
            }
        }

        //Subida de nuevo archivo
            $archivoASubir = $service->files->create($datosArchivo, array(
                'data' => $data,
                'uploadType' => 'media'
            ));

            //$actualizarArchivo = $service->files->update()
            echo ("Archivo con nombre '".$nombreArchivo."' subido con exito");

    } catch (Exception $exception) {
        echo "Codigo: ".$exception->getCode()." - Error: ".$exception->getMessage();
    }
} else {
    echo "No se subio el archivo";
}