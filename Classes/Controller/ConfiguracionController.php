<?php
namespace FranciscoYanquin\GoogleDrive\Controller;

use FranciscoYanquin\GoogleDrive\Util\GestionDrive;

ini_set('include_path', ini_get('include_path') . ';PHPExcel/');
/** PHPExcel */
include('PHPExcel.php');
/** PHPExcel_Writer_Excel2007 */
include('PHPExcel/Writer/Excel2007.php');

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Francisco Yanquin <f.yanquin01@ufromail.cl>, Ceisufro
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * ConfiguracionController
 */
class ConfiguracionController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * configuracionRepository
     *
     * @var \FranciscoYanquin\GoogleDrive\Domain\Repository\ConfiguracionRepository
     * @inject
     */
    protected $configuracionRepository = NULL;
    
    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $configuracions = $this->configuracionRepository->findAll();
        $this->view->assign('configuracions', $configuracions);
    }
    
    /**
     * action show
     *
     * @param \FranciscoYanquin\GoogleDrive\Domain\Model\Configuracion $configuracion
     * @return void
     */
    public function showAction(\FranciscoYanquin\GoogleDrive\Domain\Model\Configuracion $configuracion)
    {
        $this->view->assign('configuracion', $configuracion);
    }
    
    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {
        $tmp = 1;
    }
    
    /**
     * action create
     *
     * @param \FranciscoYanquin\GoogleDrive\Domain\Model\Configuracion $newConfiguracion
     * @return void
     */
    public function createAction(\FranciscoYanquin\GoogleDrive\Domain\Model\Configuracion $newConfiguracion)
    {
        $this->configuracionRepository->add($newConfiguracion);
        $tmp = 1;
        $this->redirect('list');
    }
    
    /**
     * action edit
     *
     * @param \FranciscoYanquin\GoogleDrive\Domain\Model\Configuracion $configuracion
     * @ignorevalidation $configuracion
     * @return void
     */
    public function editAction(\FranciscoYanquin\GoogleDrive\Domain\Model\Configuracion $configuracion)
    {
        $this->view->assign('configuracion', $configuracion);
        $tmp = 1;
    }
    
    /**
     * action update
     *
     * @param \FranciscoYanquin\GoogleDrive\Domain\Model\Configuracion $configuracion
     * @return void
     */
    public function updateAction(\FranciscoYanquin\GoogleDrive\Domain\Model\Configuracion $configuracion)
    {
        $this->configuracionRepository->update($configuracion);
        $tmp = 1;
        $this->redirect('list');
    }
    
    /**
     * action delete
     *
     * @param \FranciscoYanquin\GoogleDrive\Domain\Model\Configuracion $configuracion
     * @return void
     */
    public function deleteAction(\FranciscoYanquin\GoogleDrive\Domain\Model\Configuracion $configuracion)
    {
        $this->configuracionRepository->remove($configuracion);
        $this->redirect('list');
    }
    
    /**
     * action redirigir
     *
     * @return void
     */
    public function redirigirAction()
    {
        $id_usuario = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['google_drive']);
        $clave_usuario = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['google_drive']);
        $url_redireccion = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['google_drive']);

        $nombre_archivo = $this->settings['drive']['nombre_archivo'];
        $ubicacion_archivo = $this->settings['drive']['ubicacion_archivo'];
        $carpeta_destino = $this->settings['drive']['carpeta_destino'];
        $id_usuario = $id_usuario['id_usuario'];
        $clave_usuario = $clave_usuario['clave_usuario'];
        $url_redireccion = $url_redireccion['url_redireccion'];

        //$listadoCarpetas = new GestionDrive($id_usuario,$clave_usuario,$url_redireccion);
        //$respuesta_servicio = $listadoCarpetas->listarArchivos();
        //$carpetas = json_decode($respuesta_servicio);

        $subidaArchivo = new GestionDrive($id_usuario,$clave_usuario,$url_redireccion);
        $subidaArchivo->subirArchivo($nombre_archivo,$ubicacion_archivo,$carpeta_destino);
    }
    
    public function getErrorFlashMessage()
    {
        return 'Ocurrio un error, verifique los datos ingresados e intente nuevamente.';
    }
    
    /**
     * action search
     *
     * @return void
     */
    public function searchAction()
    {
        $searchTerm = $this->request->getArgument('searchTerm');
        if ($this->request->hasArgument('limpiar')) {
            $this->redirect('list');
        }
        $configuracions = $this->configuracionRepository->findByLike($searchTerm);
        $this->view->setTemplatePathAndFilename(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('google_drive') . 'Resources/Private/Templates/Configuracion/List.html');
        $this->view->assign('configuracions', $configuracions);
        $this->view->assign('searchTerm', $searchTerm);
    }
    
    /**
     * action generarExcel
     *
     * @return void
     */
    public function generateExcelAction()
    {
        ob_clean();
        $noInfo = '-';
        $name = 'Configuracions';
        //Crear el nuevo objeto PHPExcel
        $excel = new \PHPExcel();
        $sFecha = date('d-m-Y_H:i:s');
        // Propiedades
        $excel->getProperties()->setCreator('Extension Builder');
        $excel->getProperties()->setLastModifiedBy('Extension Builder');
        $excel->getProperties()->setTitle('Reporte_' . $name . '_' . $sFecha);
        $excel->getProperties()->setSubject('Extension Builder');
        $excel->getProperties()->setDescription('Extension Builder');
        //Añadir datos
        $excel->setActiveSheetIndex();
        //Escribir cabecera
        $excel->getActiveSheet()->SetCellValue(\PHPExcel_Cell::stringFromColumnIndex(0) . '1', 'UrlServicio');
        $excel->getActiveSheet()->SetCellValue(\PHPExcel_Cell::stringFromColumnIndex(1) . '1', 'UbicacionArchivo');
        $excel->getActiveSheet()->SetCellValue(\PHPExcel_Cell::stringFromColumnIndex(2) . '1', 'IdCarpeta');
        $excel->getActiveSheet()->SetCellValue(\PHPExcel_Cell::stringFromColumnIndex(3) . '1', 'IdCliente');
        $excel->getActiveSheet()->SetCellValue(\PHPExcel_Cell::stringFromColumnIndex(4) . '1', 'ClaveSecreta');
        $excel->getActiveSheet()->SetCellValue(\PHPExcel_Cell::stringFromColumnIndex(5) . '1', 'UrlRedireccion');
        $configuracions = $this->configuracionRepository->findAll();
        $excelRowCounter = 2;
        foreach ($configuracions as $configuracion) {
            $excel->getActiveSheet()->SetCellValue(\PHPExcel_Cell::stringFromColumnIndex(0) . $excelRowCounter, empty($configuracion->getUrlServicio()) ? $noInfo : $configuracion->getUrlServicio());
            $excel->getActiveSheet()->SetCellValue(\PHPExcel_Cell::stringFromColumnIndex(1) . $excelRowCounter, empty($configuracion->getUbicacionArchivo()) ? $noInfo : $configuracion->getUbicacionArchivo());
            $excel->getActiveSheet()->SetCellValue(\PHPExcel_Cell::stringFromColumnIndex(2) . $excelRowCounter, empty($configuracion->getIdCarpeta()) ? $noInfo : $configuracion->getIdCarpeta());
            $excel->getActiveSheet()->SetCellValue(\PHPExcel_Cell::stringFromColumnIndex(3) . $excelRowCounter, empty($configuracion->getIdCliente()) ? $noInfo : $configuracion->getIdCliente());
            $excel->getActiveSheet()->SetCellValue(\PHPExcel_Cell::stringFromColumnIndex(4) . $excelRowCounter, empty($configuracion->getClaveSecreta()) ? $noInfo : $configuracion->getClaveSecreta());
            $excel->getActiveSheet()->SetCellValue(\PHPExcel_Cell::stringFromColumnIndex(5) . $excelRowCounter, empty($configuracion->getUrlRedireccion()) ? $noInfo : $configuracion->getUrlRedireccion());
            $excelRowCounter++;
        }
        //Cambiar el nombre de la hoja
        $excel->getActiveSheet()->setTitle('Reporte');
        // We'll be outputting an excel file
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Reporte_' . $name . '_' . $sFecha . '.xlsx"');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0', false);
        header('Pragma: no-cache');
        $headers = array(
            'Pragma' => 'no-cache',
            'Expires' => '0',
            'Cache-Control' => 'no-store, no-cache, must-revalidate',
            'Cache-Control' => 'post-check=0, pre-check=0',
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition' => 'attachment; filename="Reporte_' . $name . '_' . $sFecha . '.xlsx"',
            'Content-Transfer-Encoding' => 'binary',
            'Last-Modified' => gmdate('D, d M Y H:i:s') . ' GMT'
        );
        foreach ($headers as $header => $data) {
            $this->response->setHeader($header, $data);
        }
        $this->response->sendHeaders();
        // Write file to the browser
        ob_end_clean();
        $objWriter = new \PHPExcel_Writer_Excel2007($excel);
        $objWriter->save('php://output');
        die;
    }
    
    /**
     * action generateCSV
     *
     * @return void
     */
    public function generateCSVAction()
    {
        ob_clean();
        $noInfo = '-';
        $name = 'Configuracions';
        $sFecha = date('d-m-Y_H:i:s');
        // We'll be outputting an CSV file
        header('Content-type: text/csv');
        header('Content-Disposition: attachment;filename="Reporte_' . $name . '_' . $sFecha . '.csv"');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0', false);
        header('Pragma: no-cache');
        header('Expires: 0');
        $headers = array(
            'Pragma' => 'no-cache',
            'Expires' => '0',
            'Cache-Control' => 'no-store, no-cache, must-revalidate',
            'Cache-Control' => 'post-check=0, pre-check=0',
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="Reporte_' . $name . '_' . $sFecha . '.csv"',
            'Content-Transfer-Encoding' => 'binary',
            'Last-Modified' => gmdate('D, d M Y H:i:s') . ' GMT'
        );
        foreach ($headers as $header => $data) {
            $this->response->setHeader($header, $data);
        }
        $this->response->sendHeaders();
        // Write file to the browser
        ob_end_clean();
        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');
        //Escribir cabecera
        fputcsv($output, array(
                'UrlServicio',
                'UbicacionArchivo',
                'IdCarpeta',
                'IdCliente',
                'ClaveSecreta',
                'UrlRedireccion'
            ));
        $configuracions = $this->configuracionRepository->findAll();
        foreach ($configuracions as $configuracion) {
            fputcsv($output, array(
                    empty($configuracion->getUrlServicio()) ? $noInfo : $configuracion->getUrlServicio(),
                    empty($configuracion->getUbicacionArchivo()) ? $noInfo : $configuracion->getUbicacionArchivo(),
                    empty($configuracion->getIdCarpeta()) ? $noInfo : $configuracion->getIdCarpeta(),
                    empty($configuracion->getIdCliente()) ? $noInfo : $configuracion->getIdCliente(),
                    empty($configuracion->getClaveSecreta()) ? $noInfo : $configuracion->getClaveSecreta(),
                    empty($configuracion->getUrlRedireccion()) ? $noInfo : $configuracion->getUrlRedireccion()
                ));
        }
        die;
    }
    
    /**
     * action cargarCSV
     *
     * @return void
     */
    public function cargarCSVAction()
    {
        if ($this->request->hasArgument('resultado')) {
            $this->view->assign('resultado', $this->request->getArgument('resultado'));
        }
    }
    
    /**
     * action procesarCSV
     *
     * @return void
     */
    public function procesarCSVAction()
    {
        $archivoCSV = $_FILES['archivoCSV'];
        $resultado = new \stdClass();
        switch ($archivoCSV['error']) {
            case 0:    //Si el archivo tiene extension csv se procesara
                if (strtolower(array_pop(explode('.', $archivoCSV['name']))) == 'csv') {
                    $resultado = $this->guardarCSVAction($archivoCSV['tmp_name']);
                    if (!$resultado->error) {
                        $resultado->mensaje = 'Cargado con éxito';
                    }
                } else {
                    $resultado->error = true;
                    $resultado->mensaje = 'El archivo debe ser de tipo CSV.';
                }
                break;
            case 4:    $resultado->error = true;
                $resultado->mensaje = 'Debe seleccionar un archivo para cargar.';
                break;
        }
        $args = array('resultado' => $resultado);
        $this->forward('cargarCSV', NULL, NULL, $args);
    }
    
    /**
     * guardarCSVAction
     *
     * @param $archivoCSV
     */
    private function guardarCSVAction($archivoCSV)
    {
        $resultado = new \stdClass();
        $resultado->error = false;
        $csvFile = $archivoCSV;
        ini_set('auto_detect_line_endings', true);
        //set_time_limit(0);
        $current_row = 1;
        $delimiter = $this->getDelimiter($archivoCSV);
        $count = 6;
        //Valida el delimitador de los campos
        if ($delimiter != false) {
            $handle = fopen($csvFile, 'r');
            //Verifica si se cargo el archivo
            if ($handle) {
                $arrayRegistros = array();
                while (($data = fgetcsv($handle, null, $delimiter)) !== FALSE) {
                    $campoVacio = false;
                    $campoInvalido = false;
                    $number_of_fields = count($data);
                    //Valida la cantidad de campos
                    if ($number_of_fields == $count) {
                        if ($current_row == 1) {
                            //Header line
                            for ($c = '0'; $c < $number_of_fields; $c++) {
                                $header_array[$c] = trim(utf8_encode($data[$c]));
                            }
                        } else {
                            //Data line
                            $data_array = array();
                            for ($c = '0'; $c < $number_of_fields; $c++) {
                                //Valida que no existe ningun campo vacio
                                if (empty($data[$c])) {
                                    $campoVacio = true;
                                    $resultado->error = true;
                                    $resultado->mensaje .= 'La columna ' . $header_array[$c] . ' de la fila ' . $current_row . ' esta vacia.<br>';
                                }
                                $data_array[$c] = utf8_encode($data[$c]);
                            }
                            if (!$campoVacio && !$campoInvalido) {
                                $nuevoRegistro = new \FranciscoYanquin\GoogleDrive\Domain\Model\Configuracion();
                                $nuevoRegistro->setUrlServicio($data_array[0]);
                                $nuevoRegistro->setUbicacionArchivo($data_array[1]);
                                $nuevoRegistro->setIdCarpeta($data_array[2]);
                                $nuevoRegistro->setIdCliente($data_array[3]);
                                $nuevoRegistro->setClaveSecreta($data_array[4]);
                                $nuevoRegistro->setUrlRedireccion($data_array[5]);
                                if (!$resultado->error) {
                                    array_push($arrayRegistros, $nuevoRegistro);
                                }
                            }
                        }
                    } else {
                        $resultado->error = true;
                        $resultado->mensaje .= 'La cantidad de campos en el archivo no es la esperada. ' . 'Verifique la línea ' . $current_row . '<br>';
                    }
                    $current_row++;
                }
                fclose($handle);
            }
        } else {
            $resultado->error = true;
            $resultado->mensaje = 'El delimitador del archivo es no válido. Debe ser , ó ;';
        }
        if (!$resultado->error) {
            foreach ($arrayRegistros as $registro) {
                $this->configuracionRepository->add($registro);
            }
        }
        return $resultado;
    }
    
    /**
     * getDelimiter
     * Try to detect the delimiter character on a CSV file, by reading the first row.
     *
     * @param mixed $file
     * @access public
     * @return string
     */
    private function getDelimiter($file)
    {
        $delimiter = false;
        $line = '';
        if ($f = fopen($file, 'r')) {
            $line = fgets($f);
            // read until first newline
            fclose($f);
        }
        if (strpos($line, ';') !== FALSE && strpos($line, ',') === FALSE) {
            $delimiter = ';';
        } else {
            if (strpos($line, ',') !== FALSE && strpos($line, ';') === FALSE) {
                $delimiter = ',';
            } else {
                
            }
        }
        return $delimiter;
    }

}