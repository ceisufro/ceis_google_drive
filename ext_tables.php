<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'FranciscoYanquin.' . $_EXTKEY,
	'Configuracion',
	'Conexion Drive - Configuracion'
);



\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'ConexionGoogleDrive');


	
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_googledrive_domain_model_configuracion', 'EXT:google_drive/Resources/Private/Language/locallang_csh_tx_googledrive_domain_model_configuracion.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_googledrive_domain_model_configuracion');
