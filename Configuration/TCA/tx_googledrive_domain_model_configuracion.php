<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:google_drive/Resources/Private/Language/locallang_db.xlf:tx_googledrive_domain_model_configuracion',
		'label' => 'url_servicio',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'url_servicio,ubicacion_archivo,id_carpeta,id_cliente,clave_secreta,url_redireccion,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('google_drive') . 'Resources/Public/Icons/tx_googledrive_domain_model_configuracion.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, url_servicio, ubicacion_archivo, id_carpeta, id_cliente, clave_secreta, url_redireccion',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, url_servicio, ubicacion_archivo, id_carpeta, id_cliente, clave_secreta, url_redireccion, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_googledrive_domain_model_configuracion',
				'foreign_table_where' => 'AND tx_googledrive_domain_model_configuracion.pid=###CURRENT_PID### AND tx_googledrive_domain_model_configuracion.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		
		
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		
	
		'url_servicio' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:google_drive/Resources/Private/Language/locallang_db.xlf:tx_googledrive_domain_model_configuracion.url_servicio',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
			
		),
		'ubicacion_archivo' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:google_drive/Resources/Private/Language/locallang_db.xlf:tx_googledrive_domain_model_configuracion.ubicacion_archivo',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
			
		),
		'id_carpeta' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:google_drive/Resources/Private/Language/locallang_db.xlf:tx_googledrive_domain_model_configuracion.id_carpeta',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
			
		),
		'id_cliente' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:google_drive/Resources/Private/Language/locallang_db.xlf:tx_googledrive_domain_model_configuracion.id_cliente',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
			
		),
		'clave_secreta' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:google_drive/Resources/Private/Language/locallang_db.xlf:tx_googledrive_domain_model_configuracion.clave_secreta',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
			
		),
		'url_redireccion' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:google_drive/Resources/Private/Language/locallang_db.xlf:tx_googledrive_domain_model_configuracion.url_redireccion',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
			
		),
		
	),
);