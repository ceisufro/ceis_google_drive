<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 06-08-16
 * Time: 20:27
 */

$pluginSignature = 'googledrive_configuracion';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature,
    'FILE:EXT:google_drive/Configuration/FlexForms/FF_Googledrive_configuracion.xml'
);