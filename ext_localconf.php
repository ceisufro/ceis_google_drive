<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'FranciscoYanquin.' . $_EXTKEY,
	'Configuracion',
	array(
		'Configuracion' => 'redirigir, list, show, new, create, edit, update, delete, search, generateExcel, generateCSV',
		
	),
	// non-cacheable actions
	array(
		'Configuracion' => 'redirigir, list, show, new, create, edit, update, delete, search, generateExcel, generateCSV',
		
	)
);

// Slots for EXT:social_auth
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('social_auth')) {
	$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
	$signalSlotDispatcher->connect(
			MV\SocialAuth\Service\SocialAuthenticationService::class,
			'authUser',
			FranciscoYanquin\GoogleDrive\Util\Social_auth_slot::class,
			'authUser'
	);
}

$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['redirigeLogin'] = 'EXT:google_drive/Classes/EID/Credencial/RedireccionLogin.php';
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['solicitaCredencial'] = 'EXT:google_drive/Classes/EID/Credencial/SolicitudCredencial.php';
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['subeArchivo'] = 'EXT:google_drive/Classes/EID/Credencial/SubidaDrive.php';
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['listaCarpetas'] = 'EXT:google_drive/Classes/EID/Credencial/ListadoDrive.php';
